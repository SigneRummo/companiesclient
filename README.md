# Installation and Running Instructions

## HTTP-server setup

~~~
npm install http-server -g
~~~

## Run Application

Navigate to project root folder and then...
~~~
http-server -p 8202
~~~
...or...
~~~
./runClient.sh (Linux)
~~~
...or...
~~~
runClient.bat (Windows)
~~~
...and navigate to...
http://localhost:8202/index.html


