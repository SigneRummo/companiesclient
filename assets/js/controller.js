
// Controller functions here...

function loadSessions() {
    fetchSessions().then(
        function(sessions) {
            let sessionsHTML = "";

            for(let i =0; i < sessions.length; i++) {
                sessionsHTML = sessionsHTML + `
               <tr>
               <td>
               ${sessions[i].id}
               </td>
               <td>
               ${sessions[i].date}
               </td>
                             <td>
               
               <button class="btn btn-primary" onclick="handleEditButtonClick(${sessions[i].id})">Lisa broneering</button>
               <button class="btn btn-danger" onclick="handleDeleteButtonClick(${sessions[i].id})">Kustuta broneering</button>
               <button class="btn btn-primary" onclick="handleEditButtonClick(${sessions[i].id})">Näita osalejaid</button>
               </td>
               </tr>
            
                `; 
            }
            document.getElementById("sessionsList").innerHTML = sessionsHTML;

        }
    );
}


function handleDeleteButtonClick(id){
    if (confirm("Oled sa kindel?")) {
        deleteSession(id). then(loadSessions());
    }
    
}
function handleEditButtonClick(id) {
    $("#sessionModal").modal("show");
    fetchSession(id).then(
        function(session) {
            document.getElementById("id").value = session.id;
            document.getElementById("date").value = session.date;
        
        }
    );
}
function handleEdit() {
        let session = {
id: document.getElementById("id").value,
date: document.getElementById("date").value
        };
    postSession(session).then(
        function() {
            loadSessions();
            $("#sessionModal").modal("hide");
        }
    );
}

function handleSave() {
   // if (isFormValid() === false){
   //     return;
    //}
    if (document.getElementById("id").value > 0) {
        handleEdit();}
    else {
        handleAdd();
    }

}
function handleAddButtonClick() {
    $("#sessionModal").modal("show");
            document.getElementById("id").value = null;
            document.getElementById("date").value = null;
               }

function handleAdd() {
            let session = {
    id: document.getElementById("id").value,
    date: document.getElementById("date").value,
            };
        postSession(session).then(
            function() {
                loadSessions();
                $("#sessionModal").modal("hide");
            }
        );
    }
    // Validation
function isFormValid() {
    let name = document.getElementById("name").value;
    let logo = document.getElementById("logo").value;
if (name === null || name.length < 1) {
document.getElementById("errorMessage").innerText = "Ettevõtte nimi on kohtustuslik";
document.getElementById("errorMessage").style.display = "block";
return false;
}
if (logo === null || logo.length < 1) {
    document.getElementById("errorMessage").innerText = "Ettevõtte logo on kohtustuslik";
    document.getElementById("errorMessage").style.display = "block";
    return false;
}
document.getElementById("errormessage").style.display = "none"; 
return true; 
}
