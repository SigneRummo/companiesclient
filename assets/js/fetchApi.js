
// Functions for communicating with backend-API here...

function fetchSessions() {
    return fetch(`${API_URL}/sessions`, {method: 'GET'})
    .then(response => response.json());
}

function deleteSession(id) {
    return fetch(
        `${API_URL}/session?id=${id}`,
        {
            method: 'DELETE'
        }
    );
}
    function fetchSomething() {
    return fetch(
        `${API_URL}/something`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json());
}

// session changing
function putCompany(session) {
    return fetch(
        `${API_URL}/session`, 
        {
            method: 'PUT', 
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(session)
        }
    );
}
// session adding
function postSession(session) {
    return fetch(
        `${API_URL}/session`, 
        {
            method: 'POST', 
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(session)
        }
    );
}
function postSomething(something) {
    return fetch(
        `${API_URL}/something`, 
        {
            method: 'POST', 
            body: JSON.stringify(something)
        }
    );
}
function fetchCompany(id) {
    return fetch(`${API_URL}/session?id=${id}`, {method: 'GET'})
    .then(response => response.json());
}

function deleteSomething(id) {
    return fetch(
        `${API_URL}/something/${id}`,
        {
            method: 'DELETE'
        }
    );
}
